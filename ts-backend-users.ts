import { Request, Response } from 'express';

import bcrypt from 'bcryptjs';
import passport from 'passport';
import imageSize from 'image-size';

import database from './database';
import * as strings from './strings';
import * as config from './config';
import * as regexes from './regexes';
import * as debug from './debug';

export function confirmUser(req: Request, res: Response, next: Function) {
    if(!req.user) return res.status(403).json({});
    return next();
}

export function confirmAdmin(req: Request, res: Response, next: Function) {
    if(!req.user || !req.user.admin) return res.status(403).json({});
    return next();
}

export function confirmNotBanned(req: Request, res: Response, next: Function) {
    if(!req.user || req.user.banned) return res.status(403).json({});
    return next();
}

export function getWatch(req: Request, res: Response) {
    if(req.user.id == req.params.userid) return res.status(404).json({});
    database.query('SELECT id FROM user_watches WHERE user = ? AND target = ?', [req.user.id, req.params.userid], (err, result) => {
        if(err) return res.status(500).send(debug.error(err.toString()));
        if(result.length == 0) return res.status(404).json({});
        return res.status(200).json({});
    });
}

export function watchUser(req: Request, res: Response) {
    if(req.user.id == req.params.userid) return res.status(403).json({});
    database.query('SELECT id FROM user_watches WHERE user = ? AND target = ?', [req.user.id, req.params.userid], (err, result) => {
        if(err) return res.status(500).send(debug.error(err.toString()));
        if(result.length != 0) return res.status(200).json({});
        database.query('SELECT id FROM users WHERE id = ?', req.params.user_id, (err, resultUser) => {
            if(err) return res.status(500).send(debug.error(err.toString()));
            if(resultUser.length == 0) return res.status(404).json({});
            database.query('INSERT INTO user_watches SET ?', {user: req.user.id, target: req.params.userid}, (err, _resultInsert) => {
                if(err) return res.status(500).send(debug.error(err.toString()));
                return res.status(200).json({});
            })
        })
    })
}

export function unwatchUser(req: Request, res: Response) {
    database.query('DELETE FROM user_watches WHERE user = ? AND target = ?', [req.user.id, req.params.userid], (err, _result) => {
        if(err) return res.status(500).send(debug.error(err.toString()));
        return res.status(200).json({});
    })
}

export function listUsers(req: Request, res: Response) {
    let fields = 'id, username, steamid64, facebookid, xboxid, playstationid, creation_date, location, birth_date, first_name, last_name, bio, website, contact_email, last_visited';
    if(req.user && req.user.admin) fields += ', admin, banned, email_verified';

    //TODO: Implement facebookid, and later xboxid and playstationid
    if(req.body.steamid64) {
        if(req.body.page) return res.status(422).json({});
        database.query('SELECT '+ fields +' FROM users WHERE steamid64 = ?', req.body.steamid64, (err, queryResult) => {
            if(err) return res.status(500).send(debug.error(err.toString()));
            if(queryResult.length == 0) return res.status(404).json({});
            return res.send(queryResult[0]);
        });
    }
    else if(req.body.name) {
        if(req.body.page) return res.status(422).json({});
        database.query('SELECT '+fields+' FROM users WHERE username = ?', req.body.name, (err, queryResult) => {
            if(err) return res.status(500).send(debug.error(err.toString()));
            if(queryResult.length == 0) return res.status(404).json({});
            return res.send(queryResult[0]);
        })
    }
    else {
        //if(!req.user || !req.user.admin) return res.status(403).json({});
        config.get('user_page_size').then(userPageSize => {
            let page = req.body.page ? req.body.page : 1;
            database.query('SELECT '+fields+' FROM users LIMIT ?, ?', [userPageSize.value * (page - 1), userPageSize.value], (err, queryResult) => {
                if(err) return res.status(500).send(debug.error(err.toString()));
                return res.status(200).send(queryResult);
            });
        }).catch(err => res.status(err.status).send(err));
    }
}

export function getSingleUser(req: Request, res: Response) {
    let fields = 'id, username, steamid64, facebookid, xboxid, playstationid, creation_date, location, birth_date, first_name, last_name, bio, website, contact_email, last_visited';
    if(req.user && req.user.admin) fields += ', admin, banned, email_verified';

    database.query('SELECT ' + fields + ' FROM users WHERE id = ?', req.params.userid, (err, queryResult) => {
        if(err) return res.status(500).send(debug.error(err.toString()));
        if(queryResult.length == 0) return res.status(404).json({});
        return res.status(200).send(queryResult[0]);
    });
}

export function modifyUser(req: Request, res: Response) {
    let modifications: any = {
        username: req.body.username,
        email: req.body.email,
        admin: req.body.admin,
        banned: req.body.banned,
        location: req.body.location,
        first_name: req.body.firstName,
        last_name: req.body.lastName,
        bio: req.body.bio,
        website: req.body.website,
        contact_email: req.body.contactEmail
    };

    if(!req.user.admin && (modifications.admin !== null || modifications.banned !== null || modifications.username !== null))
        return res.status(403).json({});

    Object.keys(modifications).forEach(k => (modifications[k] == undefined) && delete modifications[k]);

    database.query('UPDATE users SET ? WHERE id = ?', [modifications, req.params.userid], (err, result) => {
        if(err) return res.status(500).json({});
        if(result.affectedRows == 0) return res.status(404).json({});
        return res.status(200).json({});
    });
}

export function wipeUser(_req: Request, res: Response) {
    //TODO: Code that.
    return res.status(501).json({});
}

export function deleteUser(req: Request, res: Response) {
    if(!req.user.admin && req.params.userid != req.user.id) return res.status(403).json({});

    database.query('DELETE FROM notifications WHERE user = ?; DELETE FROM comments WHERE user = ?; DELETE FROM watches WHERE user = ?; UPDATE users SET steamid64 = NULL, username = NULL, password = NULL, email = NULL, admin = 0, banned = 0, facebookid = NULL, xboxid = NULL, playstationid = NULL, location = NULL, birth_date = NULL, first_name = NULL, last_name = NULL, avatar = NULL, bio = NULL, website = NULL, contact_email = NULL, email_verified = 0 WHERE id = ?', [req.params.userid, req.params.userid, req.params.userid, req.params.userid], (err, results) => {
        if(err) return res.status(500).send(debug.error(err.toString()));
        if(results[3].affectedRows == 0) return res.status(404).json({});
        return res.status(200).json({});
    });
}

export function confirmFirstRun(req: Request, res: Response) {
    database.query('UPDATE users SET last_visited = NOW() WHERE id = ?', req.user.id, (err, _result) => {
        if(err) return res.status(500).send(debug.error(err.toString()));
        return res.status(200).json({});
    });
}

export function loginUserWithSteam(steamid64: string, done: (err: any, user: any) => {}): any {
    if(!steamid64) return done('No Steam ID received.', null);
    return database.query('SELECT * FROM users WHERE steamid64 = ?', steamid64, (err, queryResult) => {
        if(err) return done(err, null);
        if(queryResult.length == 0) {
            return database.query('INSERT INTO users SET steamid64 = ?, admin = 0, banned = 0', steamid64, (queryError, _queryResult) => {
                if(queryError) return done(err, null);
                return loginUserWithSteam(steamid64, done);
            });
        }
        else return done(null, queryResult[0]);
    });
}

export function getMyself(req: Request, res: Response) {
	if(!req.user) return res.status(404).json({});
    database.query('SELECT id, username, steamid64, xboxid, playstationid, facebookid, admin, banned FROM users WHERE id = ?', req.user.id, (err, result) => {
        if(err) return res.status(500).send(debug.error(err.toString()));
        return res.status(200).send(result[0]);
    });
}

export function logoutUser(req: Request, res: Response) {
    if(!req.session) return res.status(200).json({});
    req.session.destroy((err: any) => {
        if(err) return res.status(500).send(debug.error(err.toString()));
        return res.status(200).json({});
    });
}

export function registerUser(req: Request, res: Response) {
    if(!req.body.username || !req.body.password || !req.body.email) return res.status(422).send(strings.getStringInternal('ERR_FILL_IN_ALL_FIELDS'));
    database.query('SELECT id FROM users WHERE email = ?', req.body.email, (err, result) => {
        if(err) return res.status(500).send(debug.error(err.toString()));

        if(result.length != 0) return res.status(403).send(strings.getStringInternal('ERR_EMAIL_ALREADY_USED'));
        if(!regexes.verifyEmail(req.body.email)) return res.status(422).send(strings.getStringInternal('ERR_EMAIL_INCORRECT'));
        if(req.body.username.length > 30) return res.status(422).send(strings.getStringInternal('ERR_USERNAME_TOO_LONG'));
        if(req.body.email.length > 64) return res.status(422).send(strings.getStringInternal('ERR_EMAIL_TOO_LONG'));
        
        const avatar = req.body.avatar ? (Buffer.from(req.body.avatar || '', 'base64') || null) : null;
        
        if(avatar) {
        	if(avatar.length > (process.env.MAX_AVATAR_SIZE || 3145728)) return res.status(413).send(strings.getStringInternal('ERR_AVATAR_TOO_BIG'));
            try {
                imageSize(avatar);
            }
            catch(e) {
                return res.status(422).send(strings.getStringInternal('ERR_AVATAR_NOT_IMAGE'));
            }
        }

        database.query('INSERT INTO users SET ?', {
            username: req.body.username,
            password: bcrypt.hashSync(req.body.password, bcrypt.genSaltSync(10)),
            email: req.body.email,
            avatar: avatar
        }, (err, result) => {
            if(err) return res.status(500).send(debug.error(err.toString()));
            return res.status(200).json({id: result.insertId});
        });
    });
}

// Function meant to be used with passportjs' LocalStrategy
export function loginUserWithPassword(username: string, password: string, done: any) {
    database.query('SELECT id, password FROM users WHERE email = ?', username, (err, result) => {
        if(err) return done(err);
        if(result.length == 0 || !bcrypt.compareSync(password, result[0].password)) return done(null, false, { message: strings.getStringInternal('ERR_INCORRECT_CREDENTIALS') });
        return done(null, result[0]);
    });
}

export function loginUser(req: Request, res: Response) {
    if(req.body.method == 'password') return passport.authenticate('local')(req, res, () => { return res.status(200).json({}) });
    if(req.body.method == 'steam') 
        return passport.authenticate('steam', { failureRedirect: '/login/fail' })(req, res, (_req: Request, res2: Response) => {
            return res2.redirect(process.env.LOGIN_REDIRECT || '');
        });
    if(req.body.method == 'facebook') return res.status(501).send(strings.getStringInternal('ERR_NOT_IMPLEMENTED'));
    if(req.body.method == 'xbox') return res.status(501).send(strings.getStringInternal('ERR_NOT_IMPLEMENTED'));
    if(req.body.method == 'playstation') return res.status(501).send(strings.getStringInternal('ERR_NOT_IMPLEMENTED'));
    return res.status(422).send(strings.getStringInternal('ERR_INCORRECT_METHOD'));
}

export function authAction(req: Request, res: Response) {
    if(req.body.action == "logout") return logoutUser(req, res);
    if(req.body.action == "register") return registerUser(req, res);
    if(req.body.action == "login") return loginUser(req, res);
    return res.status(422).send(strings.getStringInternal('ERR_INCORRECT_ACTION'));
}
