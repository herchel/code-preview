import React, { Component } from 'react';
import API from '../../../api/api';
import { Mod, Category, Tag, SiteUser, ModFile, ModComment } from '../definitions';
import Moment from 'react-moment';
import { Link } from 'react-router-dom';
import apiAddress from '../../../api/config';

import { UserContext, IUserContext } from '../Context';

interface IState {
	mod: Mod,
	tags: Tag[],
	categoryInfo: Category,
	creatorInfo: SiteUser,
	screenshotNumber: number,
	modFiles: ModFile[],
	similarMods: Mod[],
	voteAction: number,
	commentView: boolean,
	comments: ModComment[],
	updates: ModComment[],
	commentText: string,
	viewCounted: boolean,
	watched: boolean
}

declare let window: any;
const initialState: IState = {
	mod: {
		id: 0,
		creator: 0,
		hidden: 0,
		name: '',
		short_description: '',
		long_description: '',
		used_tags: [],
		category: 0,
		downloads: 0,
		views: 0,
		featured: false,
		rating: 0
	},
	tags: [],
	categoryInfo: {id: 0, name: '', slug:''},
	creatorInfo: {id: 0, username: '', admin: false, banned: false, location: '', bio: '', website: '', contact_email: '', first_name: '', last_name: ''},
	screenshotNumber: 0,
	modFiles: [],
	similarMods: [],
	voteAction: 0,
	commentView: false,
	comments: [],
	updates: [],
	commentText: '',
	viewCounted: true,
	watched: false
}

export default class ModPage extends Component<any, IState> {
	constructor(props: any) {
		super(props);
		const newState = initialState;
		newState.viewCounted = false;
		this.state = newState;
	}

	componentWillReceiveProps(nextProps: any) {
		if(this.props.match.params.id != nextProps.match.params.id) {
			this.setState(initialState, this.reloadSelf.bind(this));
		}
	}

	componentDidMount() {
		this.reloadSelf.bind(this)();
	}

	componentWillUpdate() {
		window.$('.slider').slider();
	}

	reloadSelf() {
		API.mods.getSingleView(this.props.match.params.id, !this.state.viewCounted, (res: Mod) => {
			this.setState({mod: res});
		});

		API.screenshots.getNumber(this.props.match.params.id, (res: any) => {
			this.setState({screenshotNumber: res.amount});
		}, () => {
		});

		API.mods.getFiles(this.props.match.params.id, (res: ModFile[]) => {
			this.setState({modFiles: res});
		});

		API.mods.getSimilar(this.props.match.params.id, (res: Mod[]) => {
			this.setState({similarMods: res});
		});

		API.mods.getSelfVote(this.props.match.params.id, (res: any) => {
			this.setState({voteAction: Number(res.strength)});
		});

		API.mods.getComments(this.props.match.params.id, (res: ModComment[]) => {
			this.setState({comments: res});
		});

		API.mods.getUpdates(this.props.match.params.id, (res: ModComment[]) => {
			this.setState({updates: res});
		});

		API.mods.getWatch(this.props.match.params.id, () => {
			this.setState({watched: true});
		});
	}

	showComments() {
		this.setState({commentView: true});
	}

	showUpdates() {
		this.setState({commentView: false});
	}

	watch() {
		API.mods.setWatch(this.props.match.params.id, () => {
			window.M.toast({html: 'You will be notified of this mod\'s updates!'});
			this.setState({watched: true});
		}, () => {
			window.M.toast({html: 'Please try again!'});
		});
	}

	unwatch() {
		API.mods.unsetWatch(this.props.match.params.id, () => {
			window.M.toast({html: 'You\'re no longer watching this mod!'});
			this.setState({watched: false});
		}, () => {
			window.M.toast({html: 'Please try again!'});
		});
	}

	vote(strength: number) {
		// what did you bring upon this cursed land
		const baseAction = (strength > 0 ? 'upvote' : 'downvote');
		let action = baseAction;
		if(this.state.voteAction === strength && this.state.voteAction !== 0) action = 'unvote';

		let actualStrength = strength;
		if(action === 'unvote') {
			strength = 0;
			if(baseAction === 'upvote') actualStrength = -1;
			else actualStrength = 1;
		}
		else if (this.state.voteAction != 0) actualStrength *= 2;

		API.mods.performAction(this.props.match.params.id, {action: action}, () => {
			this.setState({ voteAction: strength });
			this.setState(prevState => {
				const newMod = prevState.mod;
				newMod.rating += actualStrength;
				return { mod: newMod };
			});
		});
	}

	upvote() {
		this.vote.bind(this)(1);
	}

	downvote() {
		this.vote.bind(this)(-1);
	}

	componentDidUpdate() {
		if(this.state.categoryInfo.id === 0 && this.state.mod.category !== 0) {
			API.categories.getSingle(this.state.mod.category, (category: Category) => {
				this.setState({categoryInfo: category});
			})
		}

		if(this.state.creatorInfo.id === 0 && this.state.mod.creator !== 0) {
			API.user.getSingle(this.state.mod.creator, (user: SiteUser) => {
				this.setState({creatorInfo: user});
			})
		}

		if(this.state.tags.length === 0 && this.state.mod.used_tags && this.state.mod.used_tags.length > 0) {
			// TODO: Make this NOT O(n^2)
			// Best solution would be modifying backend to allow retrieving many tags at once
			let newTags: Tag[] = [];
			API.tags.get((tags: Tag[]) => {
				for(let tag of tags) {
					if(this.state.mod.used_tags.indexOf(tag.id) !== -1) {
						newTags.push(tag);
					}
				}
				this.setState({tags: newTags});
			});
		}
	}

	commentChange(e: any) {
		this.setState({commentText: e.target.value});
	}

	commentSend(isUpdate?: boolean, keyboardBased?: boolean) {
		return ((e: any) => {
			if(keyboardBased === true && (!e.key || e.key !== 'Enter')) return;
			API.mods.performAction(
				this.state.mod.id,
				{
					action: 'comment',
					content: this.state.commentText
				},
				() => {
					this.setState(prevState => {
						let newComments = prevState.comments;
						if(isUpdate) newComments = prevState.updates;
						newComments.unshift({
							id: 0,
							user: 0,
							modification: this.state.mod.id,
							creation_date: Date().toString(),
							username: UserContext.displayName || 'You',
							content: this.state.commentText
						});
						this.setState({commentText: ''});
						if(isUpdate) this.setState({updates: newComments});
						else this.setState({comments: newComments});
					})
				},
				() => {
					window.M.toast({html: 'Your comment wasn\'t sent, please try again!'});
				}
			);
		}).bind(this);
	}

	render() {
		return (
			<div className='container'>
				<div className='row'>
					<div className='mod-page-header col s12'> 
						<div className='mod-name'>{this.state.mod.name}</div>
						<Link to={'/'+this.state.categoryInfo.slug} className='mod-author'>{'in ' + (this.state.categoryInfo.name || 'Uncategorized')}</Link>
					</div>
				</div>
				<div className='row'>
					<div className='col s12 m12 l8 right'>
						<div className='slider'>
						<ul className='slides'>
							<li key={1}><img src={apiAddress.apiAddress + '/images/mods/' + this.state.mod.id}/></li>
							{this.state.screenshotNumber > 0 ? [...Array(this.state.screenshotNumber)].map((x, i) =>
								<li key={i}><img src={apiAddress.apiAddress + '/images/screenshots/' + this.state.mod.id + '?number=' + i}/></li>
							) : ''}
						</ul>
						</div>
						<div>
							<div className='tag-container'>{this.state.tags.map(tag =>
								<Link
									key={tag.id}
									className={'tag-display'}
									to={'/?tag='+tag.id}
								>{tag.tag}</Link>
							)}</div>
						</div>
						<div className='card-panel description-panel'>
							<p className='mod-description'>{this.state.mod.long_description}</p>
							<Link className='report-button' to={'/report/'+this.state.mod.id}>Report this mod</Link>
						</div>
						<div className='card-panel comment-panel'>
							<div>
								<a onClick={this.showUpdates.bind(this)} className={'waves-effect waves-' + (this.state.commentView ? 'blue btn-flat' : 'light blue btn')}>Updates</a>
								<a onClick={this.showComments.bind(this)} className={'waves-effect waves-' + (this.state.commentView ? 'light blue btn' : 'blue btn-flat')}>Comments</a>
							</div>
							<UserContext.Consumer>
								{context => {
									if(context.user) {
										if(context.user.id === this.state.creatorInfo.id) {
											if(!this.state.commentView) { 
												return (
													<div>
														<div className='input-field col s12 m10'>
															<input id='comment-textarea' value={this.state.commentText} onChange={this.commentChange.bind(this)} onKeyPress={this.commentSend(true, true).bind(this)} className='' placeholder='Share updates with your fans!'></input>
														</div>
														<div className='col s12 m4 right'><div className='waves-effect waves-light btn full-width-button' onKeyPress={this.commentSend(true).bind(this)}><i className='white-text material-icons right'>send</i>send</div></div>
													</div>
												);
											}
											else { return (<div></div>) }
										}
										else {
											if(this.state.commentView) {
												return (
													<div>
														<div className='input-field col s12'>
															<input id='comment-textarea' value={this.state.commentText} onChange={this.commentChange.bind(this)} onKeyPress={this.commentSend(false, true).bind(this)} className='' placeholder='Share your opinion about this mod!'></input>
														</div>
														<div className='col s12 m4 right'><div className='waves-effect waves-light btn full-width-button' onClick={this.commentSend(false).bind(this)}><i className='white-text material-icons right'>send</i>send</div></div>
													</div>
												);
											}
											else { return (<div></div>) }
										}
									}
									else if(this.state.commentView) {
										return (
											<div className='input-field col s12'>
												<textarea id='comment-textarea' className='materialize-textarea' disabled>Please log in or register to comment.</textarea>
											</div>
										);
									}
									else return(<div></div>);
								}}
							</UserContext.Consumer>
							{this.state.commentView ? ( this.state.comments.length ?
								this.state.comments.map(comment => 
									<div className='mod-comment'>
										<img className='comment-avatar' src={apiAddress.apiAddress+'/images/users/'+comment.user}/>
										<div className='comment-content'>
											<div className='comment-user'>{(comment.username || 'Deleted user')}</div>
											<div>{comment.content}</div>
										</div>
									</div>) : <div className='mod-comment'>There are no comments yet, be the first to write one!</div> )
								: 
								(this.state.updates.length ? this.state.updates.map(update => 
									<div className='mod-comment' key={update.id}>
										<img className='comment-avatar' src={apiAddress.apiAddress+'/images/users/'+update.user}/>
										<div className='comment-content'>
											<Moment fromNow className='update-date'>{update.creation_date}</Moment>
											<div>{update.content}</div>
										</div>
									</div>
									) : <div className='mod-comment'>There are no updates!</div>
								)}
							</div>
						</div>
						<div className='col s12 m12 l4'>

						{this.state.modFiles.length > 0 ?
						<a className='waves-effect waves-light btn-large full-width-button green' target='_blank' href={apiAddress.apiAddress+'/mods/'+this.state.mod.id+'/files/'+this.state.modFiles[0].version+'/download'}>
							<i className='material-icons left white-text'>save</i>
							<div className='white-text'>Download</div>
						</a> :
						<a className='waves-effect waves-light btn-large full-width-button green disabled' href='#download'>
							<i className='material-icons left grey-text'>save</i>
							<div>No download available</div>
						</a>}

						<a className='waves-effect waves-light btn-large full-width-button blue' href='#share'>
							<i className='material-icons left white-text'>share</i>
							<div className='white-text'>Share</div>
						</a>

						<UserContext.Consumer>
							{ context => { return(
								context.user ? 
								<a 
									className={'waves-effect waves-light btn-large full-width-button ' + (this.state.watched ? 'purple' : 'pink')}
									href='#watch'
									onClick={this.state.watched ? this.unwatch.bind(this) : this.watch.bind(this)}>
									<i className='material-icons left white-text'>{this.state.watched ? 'notifications_active' : 'notifications'}</i>
									<div className='white-text'>{this.state.watched ? 'Unwatch' : 'Watch'}</div>
								</a> :
								<Link className='waves-effect waves-light btn-large full-width-button pink' to='/login'>
									<i className='material-icons left white-text'>notifications</i>
									<div className='white-text'>Watch</div>
								</Link>
							) }}
						</UserContext.Consumer>

						<UserContext.Consumer>
							{context => { if(context.user && (context.user.id === this.state.creatorInfo.id || context.user.admin))
								return <div>
									<Link className='waves-effect waves-light btn-large full-width-button orange' to={'/edit/'+this.state.mod.id}>
										<i className='material-icons left white-text'>edit</i>
										<div className='white-text'>Edit</div>
									</Link>
								</div>
								else return <div></div>
							}}
						</UserContext.Consumer>

						<div className='card-panel user-card'>
							<img className='avatar-small hide-on-med-and-down' src={apiAddress.apiAddress + '/images/users/' + this.state.mod.creator} alt={this.state.creatorInfo.username}/>
							<img className='avatar-small-mobile hide-on-large-only' src={apiAddress.apiAddress + '/images/users/' + this.state.mod.creator} alt={this.state.creatorInfo.username}/>
							<div className='user-info'>
								<Link to={'/user/'+this.state.creatorInfo.id}>
									<span className='user-info-username hide-on-med-and-down'>{this.state.creatorInfo.username}</span>
									<span className='user-info-username-mobile hide-on-large-only'>{this.state.creatorInfo.username}</span>
								</Link>
								<div className='user-info-buttons'>
									<Link to={'/user/'+this.state.creatorInfo.id}><i className='material-icons'>person</i></Link>
									{this.state.creatorInfo.contact_email ? <a href={'mailto:'+this.state.creatorInfo.contact_email}><i className='material-icons'>email</i></a> : <span></span>}
									{this.state.creatorInfo.website ? <a target='_blank' href={this.state.creatorInfo.website}><i className='material-icons'>public</i></a> : <span></span>}
								</div>
							</div>
						</div>
						<div className='card-panel mod-info-card'>
							<span><i className='material-icons small'>visibility</i>{this.state.mod.views}</span>
							<span className='black-text'>
								<i onClick={this.upvote.bind(this)} className={'material-icons small' + (this.state.voteAction > 0 ? ' green-text' : '')}>thumb_up</i>
								{this.state.mod.rating}
								<i onClick={this.downvote.bind(this)} className={'material-icons small' + (this.state.voteAction < 0 ? ' red-text' : '')}>thumb_down</i>
							</span>
							<span><i className='material-icons small'>cloud_download</i>{this.state.mod.downloads}</span>
						</div>
						<div className='card-panel mod-card'>
							<div>Releases</div>
							{this.state.modFiles.length > 0 ?
								this.state.modFiles.map((file: ModFile) =>
									<div className='mod-file-list-item' key={file.id}>
										<div>
											<div>{file.name} - {file.version}</div>
											<div>{file.downloads} downloads 
												{file.verified || !file.uploaded ? <span></span> : <span className='red-text italic'> (file not verified)</span>}
											</div>
										</div>
								{file.uploaded ? <a target='_blank' href={apiAddress.apiAddress+'/mods/'+this.state.mod.id+'/files/'+file.version+'/download'}><i className='material-icons small'>save_alt</i></a> : <div></div>}
									</div>
								):
								<div className='centered-in-card-panel'>No files available</div>}
						</div>
						<div className='card-panel mod-card'>
							<div>You may also like...</div>
							{this.state.similarMods.length > 0 ?
								this.state.similarMods.map((mod: Mod) =>
									<Link className='similar-mod-list-item' to={'/mods/'+mod.id} key={mod.id}>
										<div className="list-image" style={{
											backgroundImage: 'url('+ apiAddress.apiAddress + '/images/mods/' + mod.id + ')',
											backgroundSize: 'cover',
											backgroundPosition: 'center'
										}}>
										</div>
										<div className='similar-mod-name'>{mod.name}</div>
									</Link>
								):
								<div className='centered-in-card-panel'>No similar mods</div>}
						</div>
					</div>
				</div>
			</div>
		);
	}
}
