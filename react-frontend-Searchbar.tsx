import React, { Component } from 'react';
import API from '../../../../api/api';

interface IProps {
	onSearch: (query: string) => void,
	button?: boolean,
	maxLength?: number,
	disabled?: boolean,
	withSorting?: boolean,
	onSort?: (type: string) => void,
	clearOnSearch?: boolean,
	placeholder?: string,
	id: string,
	defaultValue?: string,
	small?: boolean,
	className?: string
}

interface IState {
	query: string
}

declare let window: any;

const initialState: IState = {
	query: ''
}

export default class Searchbar extends Component<IProps, IState> {
	constructor(props: IProps) {
		super(props);
		const newState = initialState;
		newState.query = this.props.defaultValue || '';
		this.state = newState;
	}

	componentDidMount() {
		this.reloadSelf.bind(this)();
	}

	componentWillUpdate() {
		window.$('.dropdown-trigger').dropdown();
	}

	reloadSelf() {
		const newState = initialState;
		if(this.props.defaultValue) newState.query = this.props.defaultValue;
		this.setState(newState);
	}

	search() {
		this.props.onSearch(this.state.query);
		if(this.props.clearOnSearch) this.setState({ query: '' });
	}

	sort() {
	}

	handleChange(e: any) {
		this.setState({ query: e.target.value });
	}

	handleSend(e: any) {
		if(!e.key || e.key !== 'Enter') return;
		this.search.bind(this)();
	}

	clearSelf() {
		this.setState({ query: '' }, this.search.bind(this));
	}

	render() {
		return ( <div className={(this.props.small ? 'searchbar small ' : 'searchbar ') + (this.props.className || '')}>
			<input placeholder={this.props.placeholder || ''} id={this.props.id} name={this.props.id} type='text' value={this.state.query} disabled={this.props.disabled || false} onKeyPress={this.handleSend.bind(this)} onChange={this.handleChange.bind(this)}/>
			{ this.props.button ? <a className='waves-effect waves-light btn-floating blue white-text' onClick={this.search.bind(this)} href={'#searchbar-'+this.props.id}>
				<i className='white-text material-icons'>search</i>
			</a> : <div></div> }
			{ this.props.withSorting ? <a className='waves-effect waves-light btn-floating blue white-text dropdown-trigger' data-target={'sort-dropdown-'+this.props.id} href={'#searchbar-sort-'+this.props.id}>
				<i className='white-text material-icons'>sort</i>
			</a> : <div></div> }
			{ this.props.withSorting ? <ul id={'sort-dropdown-'+this.props.id} className='dropdown-content'>
				<li><a href='#!'>Newest</a></li>
				<li><a href='#!'>Downloads</a></li>
				<li><a href='#!'>Views</a></li>
				<li><a href='#!'>A-Z</a></li>
				<li><a href='#!'>Z-A</a></li>
			</ul>: <div></div>}
		</div> );
	}
}
